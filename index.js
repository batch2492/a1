let students = ["John", "Joe", "Jane", "Jessie"];
console.log(students);

students.push("Raf");
console.log(students);

students.unshift ("Pinky");
console.log(students);

students.pop();
console.log(students);

students.shift();
console.log(students);



let arrNum = [15, 20, 25, 30, 11, 7];

/*
	using forEach, check if divisible by 5.
	output : <num> is divisible by 5

*/

arrNum.forEach(check)

function check(x){

	if((x % 5) == 0){
		console.log(x + " is divisible by 5")
	}
	else{
		console.log(false)
	}


}

arrNum.forEach(num => {
	if (num % 5 === 0){
		console.log(num + " is divisible by 5")
	} else {
		console.log(false);
	}
})


//Math Object - allows to perform mathematical tasks on numbers

console.log(Math);
console.log(Math.E); // Euler's Number
console.log(Math.PI); // PI
console.log(Math.SQRT2); // square root of 2
console.log(Math.SQRT1_2); // square root of 1 half
console.log(Math.LN2); // natural logarithm of 2
console.log(Math.LOG2E); // base 2 of log of E 

console.log(Math.round(Math.PI));
console.log(Math.ceil(Math.PI));
console.log(Math.floor(Math.PI));
console.log(Math.trunc(Math.PI));

console.log(Math.min(-1, -2, -4, -100, 0, 1, 3, 5, 6));

const number = 5 ** 2;

console.log(number);




/*
Activity

1. brackets []
2. index 0
3. length - 1
4. indexOf()
5. forEach()
6. from()
7. every()
8. some()
9. True
10. True


*/

console.log("Activity Part");

/*Function Coding*/

// Number 1
console.log("Number 1:");

function addToEnd(arrSample, item){
	if( typeof item !== 'string'){
		console.log("error - can only add strings to an array");
	}
	else{
		arrSample.push(item);
		console.log(arrSample);
	}
}
console.log('addToEnd(students, "Ryan")');
addToEnd(students, "Ryan");

console.log('addToEnd(students, 045)');
addToEnd(students, 045);


// Number 2

console.log("Number 2:");


function addToStart(arrSample, item){
	if( typeof item !== 'string'){
		console.log("error - can only add strings to an array");
	}
	else{
		arrSample.unshift(item);
		console.log(arrSample);
	}
}

console.log('addToStart(students, "Tess")');
addToStart(students, "Tess");

console.log('addToStart(students, 033)');
addToStart(students, 033);



//  Number 3

console.log("Number 3:");

function elementChecker(arrSample, item){
	if( arrSample.length == 0){
		console.log("error - passed in array is empty");
	} 
	else {

		console.log(arrSample.includes(item));
	}
}

console.log('elementChecker(students, "Jane");');
elementChecker(students, "Jane");

console.log('elementChecker([], "Jane");');
elementChecker([], "Jane");



//  Number 4

console.log("Number 4:");

function checkAllStringsEnding(arrSample, item){

	let str = arrSample.every(data => {
		return typeof data == 'string';
	})

	if(!str){
		console.log("error - all array elements must be strings");
	}

	else if(arrSample.length == 0){
		console.log("error - array must NOT be empty");
		return;
	}


	else if(typeof item !== 'string'){
		console.log("error - 2nd argument must be of data type string");
		return;
	}
	else if(item.length > 1){
		console.log("error - 2nd argument must be a single character");
		return;
	}
	
	else{

		let ans = arrSample.every(variable => {
		return variable.at(-1) === item;	
			})
		
		console.log(ans);
	}

}

console.log('checkAllStringsEnding(students, "e");')
checkAllStringsEnding(students, "e");

console.log('checkAllStringsEnding([], "e");')
checkAllStringsEnding([], "e");

console.log('checkAllStringsEnding(["Jane", 02], "e");')
checkAllStringsEnding( ["Jane", 06 ], "e");

console.log('checkAllStringsEnding(students, 4);');
checkAllStringsEnding(students, 4);



// Number 5

console.log("Number 5:");

function stringLengthSorter(arrSample){

	let str = arrSample.every(data => {
		return typeof data == 'string';
	})

	if(!str){
		console.log("error - all array elements must be strings");
	} 

	else {


		arrSample.sort(function(a,b){
			if(a.length > b.length){
				return 1;
			}
			if(b.length > a.length){
				return -1;
			}
			return 0;
		})
		console.log(arrSample)

	}

}


console.log('stringLengthSorter(students);')
stringLengthSorter(students);

console.log('stringLengthSorter([037, "John", 039, "Jane"]);')
stringLengthSorter([037, "John", 039, "Jane"]);


// Number 6
console.log("Number 6:");

function startsWithCounter(arrSample, item){

	let str = arrSample.every(data => {
		return typeof data == 'string';
	})

	if(!str){
		console.log("error - all array elements must be strings");
	} 

	else if(arrSample.length == 0){
		console.log("error - array must NOT be empty");
		return;
	}
	else if(typeof item !== 'string'){
		console.log("error - 2nd argument must be of data type string");
		return;
	}
	else if(item.length > 1){
		console.log("error - 2nd argument must be a single character");
		return;
	}

	else {

		let count = 0;
		arrSample.forEach(data => {
			if(data[0].toLowerCase() == item.toLowerCase()){
				count++;
			}
		})

		console.log(count);
	}
}


console.log('startsWithCounter(students, "j");')
startsWithCounter(students, "j");


// Number 7
console.log("Number 7:");




// Number 8
console.log("Number 8:");

